import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BandejaComponent } from './bandeja/bandeja.component';



@NgModule({
  declarations: [
    BandejaComponent
  ],
  imports: [
    CommonModule
  ]
})
export class UsuarioModule { }
